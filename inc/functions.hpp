#ifndef FUNCTIONS_HPP
#define FUNCTIONS_HPP

#include <fstream>
#include <iostream>
#include <string>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include <vector>
#include <algorithm>
#include "../inc/map.hpp"
#include "../inc/player.hpp"
#include "../inc/ship.hpp"
#include "../inc/canoa.hpp"
#include "../inc/submarino.hpp"
#include "../inc/pavioes.hpp"
#include "../inc/basic_interface.hpp"

class Functions : public Basic_Interface
{
    protected:
        std::vector <std::string> text_line; //global vector
    public:
        Functions();

        ~Functions();

        void criarJogador(Player *player);
        //Criar jogador por ponteiro

        Canoa criarCanoa(Map *map);
        //Ferramenta para criação de canoas

        Submarino criarSubmarino(Map *map);
        //Ferramenta para criação de submarinos
        
        Pavioes criarPavioes(Map *map);
        //Ferramenta para criação de porta-avioes

        void listFiles(std::string tipo);
        //lista os arquivos do tipo selecionado

        bool verificaPosicao(Ship ship, Map map);
        //testa se a posicao é valida
};

#endif