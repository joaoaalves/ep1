#ifndef SHIP_HPP
#define SHIP_HPP

#include <string>
#include <iostream>

class Ship{
    private:
        std::string name;
        std::string info;
        std::string direction;
        int size;
        int x;
        int y;
        int durability;
    public:
        Ship();
        ~Ship();

        void setSize(int size);
        int getSize();

        void setDirection(std::string direction);
        std::string getDirection();

        void setName(std::string name);
        std::string getName();

        void setInfo(std::string info);
        std::string getInfo();

        void setX(int x);
        int getX();

        void setY(int y);
        int getY();

        void setDurability(int durability);
        int getDurability();
};

#endif