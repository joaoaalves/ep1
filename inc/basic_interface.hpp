#ifndef BASIC_INTERFACE_HPP
#define BASIC_INTERFACE_HPP

#include <string>
#include <vector>
#include <iostream>
#include <string.h>
#include <unistd.h>


class Basic_Interface{
    public:
        Basic_Interface();

        ~Basic_Interface();

        void screenClear(); 
        //função para limpar tela em qualquer OS

        void showAviso();
        //imprime o aviso para maximizar a tela

        void printLogo();
        //função de interface

        void printLinhaMenu(std::string line, std::string orientacao);
        //função de interface

        void printRodaPe(int linhas);
        //função de interface

        void printEmptyLine(int linhas);
        //função de interface

        void printAll(std::vector<std::string> text,std::string direcao, int num_linhas);
        //printa toda a interface utilizando varias strings

        void printAll(std::string line);
        //printa toda a interface com apenas uma string
};

#endif