#include <../inc/pavioes.hpp>

Pavioes::Pavioes(){
    setName("Porta-avioes");
    
    setDurability(4);

    setSize(4);

    setInfo("O porta-aviões é uma embarcação de grande porte que serve para abrigar aviões em alto mar");

    setHability("O porta-avioes pode aleatoriamente destruir qualquer ataque inimigo");

}

Pavioes::Pavioes(std::string direction, int x, int y){
    Pavioes();

    setDirection(direction);

    setX(x);

    setY(y);
}

Pavioes::~Pavioes(){}

void Pavioes::setHability(std::string hability){
    this-> hability = hability;
}

std::string Pavioes::getHability(){
    return hability;
}
