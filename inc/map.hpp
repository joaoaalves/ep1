#ifndef MAP_HPP
#define MAP_HPP

#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string.h>
#include <vector>
#include <map>
#include <unistd.h>
#include <../inc/pavioes.hpp>
#include <../inc/canoa.hpp>
#include <../inc/submarino.hpp>
#include <../inc/player.hpp>
#include <../inc/ship.hpp>

class Map{
    private:
        std::string name;
        int lines;
        int columns;
        std::map < std::pair <int , int>, std::string> shipsmap;
        Player player;

    public:
        Map(std::string name, int lines, int columns); //Construtor
        Map();
        ~Map(); //Destrutor
        
        void setLines(int lines);
        int getLines();
        
        void setColumns(int columns);
        int getColumns();
        
        void setName(std::string name);
        std::string getName();
        
        void setShip(Ship ship);
        std::string getShip(int x, int y);
        std::string damageShip(int x, int y);

        void setPlayer(std::string nome);
        void setPlayer(Player player);
        Player getPlayer();
        std::string getPlayerName();
        
        void genMap(std::string file_name);
        void saveMap(std::vector<Ship> ships);
};

#endif