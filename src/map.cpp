#include "../inc/map.hpp"


Map::Map(std::string name, int lines, int columns){
    setName(name);
    setLines(lines);
    setColumns(columns);
}

Map::Map(){
    setName("Unamed");
    setLines(0);
    setColumns(0);
}

Map::~Map(){
}

void Map::setName(std::string name){
    this->name = name;
}

std::string Map::getName(){
    return name;
}

void Map::setLines(int lines){
    this->lines = lines;
}

int Map::getLines(){
    return lines;
}

void Map::setColumns(int columns){
    this->columns = columns;
}

int Map::getColumns(){
    return columns;
}

void Map::setPlayer(Player player){
    this-> player = player;
}

void Map::setPlayer(std::string nome){
    player.setNome(nome);
}

Player Map::getPlayer(){
    return player;
}

std::string Map::getPlayerName(){
    return player.getNome();
}

void Map::setShip(Ship ship){
    if(ship.getName() == "Canoa")
        shipsmap[{ ship.getX(), ship.getY() }] = ship.getName();

    else
        for(int x = ship.getX(), y = ship.getY(), i = 0; i < ship.getSize(); i++)
        {    
            shipsmap[{ x, y}] = ship.getName();

            if(ship.getDirection() == "direita")
                x++;

            if(ship.getDirection() == "esquerda")
                x--;

            if(ship.getDirection() == "cima")
                y--;

            if(ship.getDirection() == "baixo")
                y++;
        }
}

void Map::genMap(std::string file_name){
    std::ifstream file("doc/" + file_name + ".txt");
    
    std::string word, name;
    
    int lines, columns, shipnumber, axisX, axisY;

    Ship genericShip;

    if(!file)

        std::cout << "Arquivo não encontrado!" << std::endl;

    else
    {
        while(getline(file, word))
            if(word[0] == '#' && word[strlen( word.c_str() ) - 1] == ':')
                break;

        file >> word;

        player.setNome(word);

        while(getline(file, word))
            if(word[0] == '#' && word[strlen( word.c_str() ) - 1] == ':')
                break;


        std::stringstream ss_lines;
        
        file >> word;
        
        ss_lines.str(word);
        
        ss_lines >> lines;  


        while(getline(file, word))
            if(word[0] == '#' && word[strlen( word.c_str() ) - 1] == ':')
                break;

        std::stringstream ss_columns;
        
        file >> word;
        
        ss_columns.str(word);
        
        ss_columns >> columns;

         
        while(getline(file, word))
            if(word[0] == '#' && word[strlen( word.c_str() ) - 1] == ':')
                break;
                       
        std::stringstream ss_shipnumber;

        file >> word;

        ss_shipnumber.str(word);

        ss_shipnumber >> shipnumber;      

        while(getline(file, word))
            if(word[0] == '#' && word[strlen( word.c_str() ) - 1] == ':')
                break;

        for(int i = 0; i < shipnumber; i++) 
        {

            file >> word;

            std::stringstream ss_x;

            ss_x.str(word);

            ss_x >> axisX;

            
            file >> word;

            std::stringstream ss_y;

            ss_y.str(word);

            ss_y >> axisY;

            file >> word;

            genericShip.setName(word);

            if(genericShip.getName() == "Canoa")
                genericShip.setSize(1);

            if(genericShip.getName() == "Submarino")
                genericShip.setSize(2);

            if(genericShip.getName() == "Porta-avioes")
                genericShip.setSize(4);

            file >> word;

            genericShip.setDirection(word);

            genericShip.setX(axisX);
            
            genericShip.setY(axisY);
            
            setShip(genericShip);

        }

        file.close();

        file_name.erase(strlen(file_name.c_str())-4); //retira o ".txt" do nome

        setName(file_name);

        setLines(lines);

        setColumns(columns);

    }

}

void Map::saveMap( std::vector<Ship> ships ){
    std::ofstream file;

    file.open("doc/" + getName() + ".txt");

    file << "# Player:" << std::endl;

    file << player.getNome() << std::endl << std::endl;

    file << "# Linhas:" << std::endl;

    file << getLines() << std::endl << std::endl;

    file << "# Colunas:" << std::endl;

    file << getColumns() << std::endl << std::endl;

    file << "# Numero de embarcações:" << std::endl;

    file << ships.size() << std::endl << std::endl;
    
    file << "# Embarcações:" << std::endl;

    for(unsigned int i = 0; i < ships.size(); i++)
        file << ships[i].getX() << " " << ships[i].getY() << " " <<  ships[i].getName() << " " << ships[i].getDirection() << std::endl; 
        
}        

std::string Map::getShip(int x, int y)
{

    return shipsmap[{ x, y }];

}

std::string Map::damageShip(int x, int y){
    std::string damagedShip;
    if(shipsmap[{ x,y }] == "Canoa")
    {
        damagedShip = getShip(x,y);

        shipsmap[{ x, y}] = "";

    }
    
    if(shipsmap[{ x, y}] == "Submarino")
    {
        damagedShip = getShip(x,y);

        shipsmap[{ x, y}] = "DanSubmarino"; //Transforma o submarino em um submarino danificado (por ter durabilidade 2)
    }

    if(shipsmap[{ x, y}] == "DanSubmarino")
    {
        damagedShip = "DanSubmarino";

        shipsmap[{ x, y}] = "";//Destroi o submarino que estiver danificado ja
    
    }
    if(shipsmap[{ x, y}] == "Porta-avioes")
    {
        srand( time(NULL) );

        srand(rand());
        
        if(rand() % 2 == 1)
        {
            damagedShip = getShip(x,y);

            shipsmap[{ x,y}] = "";
        }
        
        else
        {
            damagedShip = "DESTRUIDO";
        }
    
    }

    return damagedShip;
}