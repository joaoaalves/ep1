#ifndef CANOA_HPP
#define CANOA_HPP

#include <../inc/ship.hpp>

class Canoa : public Ship{
    public:
        Canoa();
        Canoa(int x, int y);
        ~Canoa();
};

#endif