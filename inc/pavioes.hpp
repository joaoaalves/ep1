#ifndef PAVIOES_HPP
#define PAVIOES_HPP

#include <../inc/ship.hpp>

class Pavioes : public Ship{
    private:

        std::string hability;

    public:
        Pavioes();
        Pavioes(std::string direciton, int x, int y);
        ~Pavioes();

        void setHability(std::string hability);
        std::string getHability();

};

#endif