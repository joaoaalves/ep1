#include "../inc/ship.hpp"

Ship::Ship(){}

Ship::~Ship(){}

void Ship::setSize(int size){
    this-> size = size;
}

int Ship::getSize(){
    return size;
}

void Ship::setDirection(std::string direction){
    this-> direction = direction;
}

std::string Ship::getDirection(){
    return direction;
}

void Ship::setX(int x){
    this-> x = x;
}

int Ship::getX(){
    return x;
}
void Ship::setY(int y){
    this -> y = y;
}

int Ship::getY(){
    return y;
}

void Ship::setDurability(int durability){
    this-> durability = durability;
}

int Ship::getDurability(){
    return durability;
}

void Ship::setName(std::string name){
    this-> name = name;
}

std::string Ship::getName(){
    return name;
}

void Ship::setInfo(std::string info){
    this-> info = info;
}

std::string Ship::getInfo(){
    return info;
}