#include <../inc/menu.hpp>

Menu::Menu(){}

Menu::~Menu(){}



void Menu::showInfo(){

    text_line.push_back("   Um simples jogo de batalha naval, desenvolvido para o primeiro exercicio da materia de");

    text_line.push_back("orientacao a objetos.");

    text_line.push_back(" "); //empty line
    
    text_line.push_back("   O jogo conta com o modo classico que conta no rank de cada jogador, vale lembrar que");

    text_line.push_back("todos os jogadores ficarao armazenados em um arquivo de ranking.");
    
    text_line.push_back(" "); //empty line
    
    text_line.push_back("   Existe a possibilidade de criacao de mapas in-game, ou por leitura de arquivos '.txt'. Os");
    
    text_line.push_back("mapas criados in-game sao salvos na pasta doc/ dentro da pasta do jogo.");
    
    text_line.push_back(" "); //empty line
    
    printAll(text_line, "esquerda", text_line.size());

    text_line.clear();

    std::cout << "                      Pressione enter para voltar ao menu!";

    std::cin.ignore(1024, '\n');

    std::cin.get();

}



void Menu::salvarNovoJogador(){ //Cria jogador e salva na pasta doc/
    Player player;

    screenClear();


    printAll("Digite o nome do player.");

    std::cout << "                      Nome do player: ";
    std::string nome;
    std::cin >> nome;

    player.setNome(nome);

    player.salvarPlayer();

    screenClear();

    printAll("Jogador criado com sucesso!");

    usleep(1500000);   
}



void Menu::showMenu(){

    screenClear();

    text_line.push_back("1 - Iniciar jogo");

    text_line.push_back("2 - Criar jogador");

    text_line.push_back("3 - Ver ranking");

    text_line.push_back("4 - Creditos");

    text_line.push_back("5 - Ajuda");

    text_line.push_back("6 - Sair");

    printAll(text_line,"centralizado", text_line.size());

    text_line.clear();

    std::cout << "                                                              Escolha: ";
}


void Menu::showAjuda(){

    char op ='z';
    
    text_line.push_back("   O jogo consiste em destruir todas as embarcacoes do inimigo fazendo ataques a cada rodada.");
    text_line.push_back("");

    text_line.push_back("   Na sua rodada, voce vera um mapa com seus ataques feitos, cada simbolo significa algo,");
    text_line.push_back("retratado na legenda a seguir:");
    text_line.push_back("~ -> Representa a agua.");
    text_line.push_back("0 -> Navio ou parte dele foi destruida.");
    text_line.push_back("X -> Mostra que voce errou o ataque.");
    text_line.push_back("! -> Aparece quando acertou um submarino e por ter durabilidade 2, ele ainda esta la.");
    text_line.push_back("? -> O ataque pode ser destruido pelo porta-avioes, esse simbolo representa isso. ");

    text_line.push_back("");

    text_line.push_back("Existem 3 embarcacoes:");
    text_line.push_back("Canoa - Possui tamanho de uma unidade e durabilidade igual a 1, sem nenhuma habilidade.");
    text_line.push_back("Submarino - Tamanho de 2 unidades, durabilidade de 2 por parte por conta de sua habilidade.");
    text_line.push_back("Porta-avioes - Tamanho de 4 unidades, durabilidade de 1 por parte e habilidade de destruir");
    text_line.push_back("aleatoriamente ataques antes de acertar a embarcacao.");

    while(op != 'v' && op != 'p')
    {

    printAll(text_line, "esquerda", text_line.size());

    std::cout << "                      Digite (v) para voltar ou (p) para proxima pagina: ";

    std::cin >> op;

    }

    text_line.clear();

    if(op == 'p')
    {
        text_line.push_back("   O jogo possui sistema de criacao de mapas in-game e consegue ler mapas por arquivos txt,");
        text_line.push_back("cada mapa esta vinculado a um player, e esse player pode ter varios mapas, porem nao o contraio.");
        
        text_line.push_back("");
        
        text_line.push_back("   Contamos tambem com um sistema de ranking onde quem tiver maior numero de vitorias estara mais");
        text_line.push_back("perto do podio!");

        printAll(text_line, "esquerda", text_line.size());

        text_line.clear();

        std::cout << "                      Pressione enter para voltar ao menu!";

        std::cin.ignore(1024, '\n');

        std::cin.get();

    }


}

void Menu::newGame(){
        int escolha;

        screenClear();

        text_line.push_back("1 - Abrir mapa existente");

        text_line.push_back("2 - Criar mapa");

        text_line.push_back("3 - Apagar mapa");

        text_line.push_back("4 - Sair");

        printAll(text_line,"centralizado",4);

        text_line.clear();

        std::cout << "                       Escolha: ";

        std::cin >> escolha;

    if(escolha == 1)
    {
        std::string map1_name, map2_name;

        Map map1, map2;

        screenClear();

        printAll("Escolha o mapa do player 1!");
        
        usleep(1500000);

        listFiles("Map");

        std::cout << "                       Mapa: ";

        std::cin >> map1_name;

        map1.genMap(map1_name);



        screenClear();

        printAll("Escolha o mapa do player 2!");

        usleep(1500000);

        listFiles("Map");

        std::cout << "                       Mapa: ";
        
        std::cin >> map2_name;

        map2.genMap(map2_name);

        startGame(&map1, &map2);
    }

    if(escolha == 2)
    {
        std::string nome;
        Map map;
        Player player;
        std::vector<Ship> ships;

        int linhas,colunas,num_canoas,num_submarinos,num_pavioes,i,opcao;

        
        
        printAll("Crie o mapa sem repetir os seguintes nomes.");
        
        usleep(1500000);
        
        listFiles("Map");
        
        std::cout << "                      Digite o nome do mapa: ";
        
        std::cin >> nome;
        
        map.setName(nome);

        
        
        screenClear();
        
        printAll("Digite o numero de linhas.");
        
        std::cout << "                      Numero de linhas: ";
        
        std::cin >> linhas;
        
        map.setLines(linhas);

        
        
        screenClear();
        
        printAll("Digite o numero de colunas.");
        
        std::cout << "                      Numero de colunas: ";
        
        std::cin >> colunas;
        
        map.setColumns(colunas);

        
        
        screenClear();
        
        text_line.push_back("1 - Criar novo player");
        
        text_line.push_back("2 - Usar player existente");

        printAll(text_line,"centralizado",text_line.size());
        
        std::cout << "                      Escolha: ";
        
        std::cin >> opcao;
        
        text_line.clear();

        
        
        if(opcao ==  1)
        {
            criarJogador( &player );
        
            map.setPlayer( player );
            
        }
        if(opcao == 2)
        {
            listFiles("Player");
            
            std::cout << "                      Digite o player selecionado: ";
            
            std::cin >> nome;

            map.setPlayer(nome);
        }
        
        
        screenClear();
        
        printAll("Digite a quantidade de canoas:");
        
        std::cout << "                      Numero de canoas: ";
        
        std::cin >> num_canoas;
        
        for(i = 0; i < num_canoas; i++)
            ships.push_back( criarCanoa( &map ));     

        
        
        screenClear();
        
        printAll("Digite a quantidade de submarinos:");
        
        std::cout << "                      Numero de submarinos: ";
        
        std::cin >> num_submarinos;
        
        for(i = 0; i < num_submarinos; i++)
            ships.push_back( criarSubmarino( &map ));

        
        
        screenClear();
        
        printAll("Digite a quantidade de porta-avioes:");
        
        std::cout << "                      Numero de porta-avioes: ";
        
        std::cin >> num_pavioes;
        
        for(i = 0; i < num_pavioes; i++)
            ships.push_back( criarPavioes( &map ));

        
        map.saveMap( ships );

        screenClear();    

        printAll("Mapa criado com sucesso!");

        newGame();
    }
    
    if(escolha == 3)
    {
        screenClear();

        listFiles("Map");

        std::cout << "                      Digite o nome do mapa a se remover: ";
        
        std::string nome;
        
        std::cin >> nome;
        
        
        nome = "doc/" + nome + ".txt";
        
        int  i = remove(nome.c_str());
        
        screenClear();

        if(i == -1)
        
            printAll("Mapa inexistente.");
        
        else

            printAll("Mapa destruido com sucesso!");
        
        usleep(1200000);
    }
    if(escolha > 4)
        newGame();

}



//Aqui o jogo vai acontecer
//É carregado 2 mapas e 2 players
//E é gerado o mapa in game com os arquivos salvos

void Menu::startGame(Map *map1, Map *map2){

    int x,y;
    screenClear();
    std::map<std::pair<int, int>, int> maptent1, maptent2;
    
    setMapaTentativas(maptent1, *map2);
    setMapaTentativas(maptent2, *map1);

    printAll("Jogo comecando!");

    usleep(1500000);

    do{
        
        screenClear();
        
        printAll("Vez do " + map1->getPlayerName() + " de jogar!");

        usleep(1500000);

        screenClear();

        printMapaTentativas(maptent1, *map2);

        std::cout << "                          Digite a cordenada X: ";

        std::cin >> x;

        std::cout << "                          Digite a coordenada Y: ";

        std::cin >> y;

        ataque(map2 , x, y, &maptent1);

        if(!verificaBarcos(*map2))
        {
            winGame(map1->getPlayer(), map2->getPlayer());
            
            break;
        }

        
        screenClear();
        
        printAll("Vez do " + map2->getPlayerName() + " de jogar!");

        usleep(1500000);

        screenClear();

        printMapaTentativas(maptent2, *map1);

        std::cout << "                          Digite a coordenada X: ";

        std::cin >> x;

        std::cout << "                          Digite a coordenada Y: ";

        std::cin >> y;

        ataque(map1 , x, y, &maptent2);


    }while(verificaBarcos(*map1));
    if(!verificaBarcos(*map1))
    {
        winGame(map2->getPlayer(), map1->getPlayer());
    }
}

bool Menu::verificaBarcos(Map map){
    int ver = 0;
    
    for(int i = 0; i < map.getLines() ; i++){
        
        for(int j = 0; j  < map.getColumns(); j++)
        
            if(map.getShip(i,j) == "")
        
                ver++;
    
    }

    if(ver == map.getLines() * map.getColumns())
    {   
        return false;
    }
    else
    {
        return true;
    }
}

void Menu::ataque(Map *map, int x, int y, std::map<std::pair<int, int>, int> *maptent)
{
    if(map->getShip( x , y ) != "")
        {
            std::string boat = map->damageShip(x, y);
            if( boat == "Canoa" || boat == "DanSubmarino" || boat == "Porta-avioes")
            {
                screenClear();

                printAll("Embarcacao acertada e destruida!");

                (*maptent) [{ x , y}] = 1;

                usleep(1500000);
            }
            if(boat == "Submarino"){
                screenClear();

                printAll("Embarcacao acertada, mas me parece que ainda resta algo la!");

                (*maptent) [{ x , y}] = 2;

                usleep(1500000);

            }
            else if(boat == "DESTRUIDO") 
            {
                screenClear();

                printAll("Estranho, me pareceu que acertaria, mas nao houve explosao!");

                (*maptent) [{ x , y}] = 3;

                usleep(1500000);

            }
        }

        else
        {
            screenClear();

            printAll("Infelizmente voce errou!");

            (*maptent) [{ x , y}] = -1;

            usleep(1500000);

        }

}

void Menu::setMapaTentativas(std::map<std::pair<int, int>, int> maptent, Map map){
    for(int i = 0; i < map.getLines(); i++){
        for(int j = 0; j < map.getColumns(); j++)
        {
            maptent[{ i, j}] = 0;
        }
    }

}

void Menu::printMapaTentativas(std::map<std::pair<int, int>, int> maptent, Map map)
{
    int coluna = map.getColumns(), linha = map.getLines();
    std::vector<std::string> linha1;
	std::string num;

    screenClear();

    std::cout <<"                         ╔══════";
    
    for(int i = 0; i < coluna * 4; i++)
        std::cout << "═";

    std::cout << "═╗\n";


	linha1.push_back("                         ║      ");

	for(int i = 0; i < coluna; i++)
    {

		num = std::to_string(i);

		if(i < 10)
			linha1.push_back(num + "  ");

		else
			linha1.push_back(num + " ");

	}
	
    linha1.push_back("║\n");


   	linha1.push_back("                        ║      ");
	
    for(int i = 0; i < coluna; i++)
		linha1.push_back("__ ");
	
	linha1.push_back("║\n");
	
    for(int i = 0; i < linha ; i++)
	{
		num = std::to_string(i);

		if(i < 10)		

			linha1.push_back("                        ║ " + num  + "  | ");

		else

			linha1.push_back("                        ║ " + num + " | ");

		for(int j = 0; j < coluna; j++)
        {
            if(maptent [{ j, i}] == 0)
                linha1.push_back("~~ ");

            if(maptent [{ j, i}] == 1)
                linha1.push_back("OO ");

            if(maptent [{ j, i}] == 2)
                linha1.push_back("!! ");

            if(maptent [{ j, i}] == 3)
                linha1.push_back("?? ");

            if(maptent [{ j, i}] == -1)
                linha1.push_back("XX ");
        }

		linha1.push_back("║\n");
	}
	for (std::vector<std::string>::const_iterator i = linha1.begin(); i != linha1.end(); ++i)
	    std::cout << *i << ' ';

    std::cout <<"                        ╚══════";
    
    for(int i = 0; i < coluna * 4; i++)
        std::cout << "═";

    std::cout << "═╝\n";
}

void Menu::winGame(Player vencedor, Player perdedor)
{
    
    screenClear();
    
    printAll(vencedor.getNome() + " venceu o jogo!");

    usleep(1500000);

    std::ifstream rank("doc/ranking.rkg");

    if(!rank)
    {
        std::ofstream rank("doc/ranking.rkg");

        rank << "# O arquivo de ranking é composto do nome de cada jogador\n";
        rank << "# e a posição dependendo da quantidade de vitórias e derrotas.\n";
        
        rank << "# Numero de players:\n";
        rank << "2\n";
        
        rank << "# Posição\tNome\tVitórias\tDerrotas\n";

        rank << "1\t\t\t" << vencedor.getNome() << "\t\t" << "1\t\t\t" << "0\n"; 
        rank << "2\t\t\t" << perdedor.getNome() << "\t\t" << "0\t\t\t" << "1\n"; 
        
    }

    else
    {
        std::string word;
        int playersnum, posicao, vitorias, derrotas, i;
        bool winver = false, loosever = false;
        std::vector<Player> players;
    
        while( getline(rank, word) )
            if(word [0] != '#') 
                break;
        
        std::stringstream ss_playersnum(word);

        ss_playersnum >> playersnum;


        while( rank >> word)
            if(word == "#")
                getline(rank, word);
            else
                break;

        for(i = 0; i < playersnum; i++)
        {
            Player genericPlayer;

            std::stringstream ss_posicao(word);
            ss_posicao >> posicao;

            genericPlayer.setPosicao(posicao);

            rank >> word;

            genericPlayer.setNome(word);

            rank >> word;

            std::stringstream ss_vitorias(word);
            ss_vitorias >> vitorias;

            if(genericPlayer.getNome() == vencedor.getNome())
            {
                vitorias++;

                winver = true;
            }
            genericPlayer.setJgsganhos(vitorias);

            rank >> word;

            std::stringstream ss_derrotas(word);
            ss_derrotas >> derrotas;

            if(genericPlayer.getNome() == perdedor.getNome())
            {    
                derrotas++;

                loosever = true;
            }
            genericPlayer.setJgsPerdidos(derrotas);

            players.push_back(genericPlayer);

            rank >> word;
        }

        if(!winver)
        {
            vencedor.setJgsganhos(1);
            vencedor.setJgsPerdidos(0);
            vencedor.setPosicao(99);
            players.push_back(vencedor);
            i++;
        }
        if(!loosever)
        {
            perdedor.setJgsganhos(0);
            perdedor.setJgsPerdidos(1);
            perdedor.setPosicao(0);
            players.push_back(perdedor);
        }
        rank.close();

        std::ofstream rank("doc/ranking.rkg");

        rank << "# O arquivo de ranking é composto do nome de cada jogador\n";
        rank << "# e a posição dependendo da quantidade de vitórias e derrotas.\n";
        
        rank << "# Numero de players:\n";
        rank << players.size() << std::endl;

        rank << "# Posição\tNome\tVitórias\tDerrotas\n";

        Player sortplayer;

        for(unsigned j = 1 ; j < players.size() ; j++){

            for(unsigned k = 0; k < players.size() -1; k++)
            {    
                if(players.at(k).getJgsganhos() < players.at(k+1).getJgsganhos())
                {
                    sortplayer = players.at(k);

                    players.at(k) = players.at(k + 1);

                    players.at(k).setPosicao(k+1);

                    players.at(k + 1) = sortplayer;
                }

                else
                {
                    players.at(k).setPosicao(k+1);
                }
                
            }      
        }

        players.at(players.size() - 1).setPosicao(players.size());


        for(unsigned j = 0; j < players.size() ; j++)
        {
        if(strlen(players.at(j).getNome().c_str()) <= 3)
            rank << players.at(j).getPosicao() << "\t\t\t" << players.at(j).getNome() << "\t\t\t" << players.at(j).getJgsganhos() << "\t\t\t" << players.at(j).getJgsperdidos() << std::endl;
        else
            rank << players.at(j).getPosicao() << "\t\t\t" << players.at(j).getNome() << "\t\t" << players.at(j).getJgsganhos() << "\t\t\t" << players.at(j).getJgsperdidos() << std::endl;
        }

        rank.close();      
        
        
        //ATE AQUI FOI PARA ALTERAR O ARQUIVO RANKING, AGORA ALTERAREI O ARQUIVO DE PLAYER
        //
        //
        //

        std::ifstream p1("doc/" + vencedor.getNome() + ".player");
        int totaljgs;

        while(p1 >> word)
            if(word == "#")
                getline(p1,word);
            else
                break;
        
        
        std::stringstream ss_vtrs(word);
        
        ss_vtrs >> vitorias;

        vitorias++;

        vencedor.setJgsganhos(vitorias);

        while(p1 >> word)
            if(word == "#")
                getline(p1,word);
            else
                break;

        std::stringstream ss_totaljogos(word);    
    
        ss_totaljogos >> totaljgs;

        totaljgs++;

        vencedor.setTotalJgs(totaljgs);

        p1.close();

        std::ofstream player("doc/" + vencedor.getNome() + ".player");

        player << "# Jogos ganhos:\n";
        player << vencedor.getJgsganhos() << std::endl << std::endl;

        player << "# Total de jogos:\n";
        player << vencedor.getTotalJgs();

        player.close();
        
        //PERDEDOR

        std::ifstream p2("doc/" + perdedor.getNome() + ".player");

        while(p2 >> word)
            if(word == "#")
                getline(p2,word);
            else
                break;
        
        std::stringstream ss_vitorias(word);
        
        ss_vitorias >> vitorias;

        perdedor.setJgsganhos(vitorias);

        while(p2 >> word)
            if(word == "#")
                getline(p2,word);
            else
                break;

        std::stringstream ss_ttljogos(word);    
    
        ss_ttljogos >> totaljgs;

        totaljgs++;

        perdedor.setTotalJgs(totaljgs);

        p2.close();

        std::ofstream player2("doc/" + perdedor.getNome() + ".player");

        player2 << "# Jogos ganhos:\n";
        player2 << perdedor.getJgsganhos() << std::endl << std::endl;

        player2 << "# Total de jogos:\n";
        player2 << perdedor.getTotalJgs();

        player2.close();

    }
    


}

void Menu::showRanking(){
    std::ifstream rank("doc/ranking.rkg");
    std::string word, num;
    std::vector<std::string> players;

    if(!rank)
    {
        printAll("Arquivo de ranking nao encontrado!");
        usleep(1200000);
    }
    else
    {
        while(rank >> word)
            if(word != "1")
                getline(rank, word);
            else
                break;
        
        for(int i = 0; i < 10; i++)
        {

        rank >> word;
        
        players.push_back(word);

        getline(rank, word);

        rank >> word;
        
        if(rank.eof())
            break;
        }

        for(unsigned i = 0; i < players.size(); i++)
        {
            num = std::to_string(i + 1);

            text_line.push_back(num + " - " + players.at(i));

        }
        screenClear();

        printAll(text_line, "centralizado", text_line.size());

        text_line.clear();

        
        std::cout << "                      Pressione enter para voltar ao menu!";
        
        std::cin.ignore(1024, '\n');

        std::cin.get();
    }
        
}   