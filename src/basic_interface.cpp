#include <../inc/basic_interface.hpp>

Basic_Interface::Basic_Interface(){}

Basic_Interface::~Basic_Interface(){}

void Basic_Interface::printLogo()
{
    std::cout << "                      ╔═══════════════════════════════════════════════════════════════════════════════════════════════════╗" << std::endl;
    std::cout << "                      ║   ooooooo                               o@o                o000oo    o@o         oo               ║" << std::endl;
    std::cout << "                      ║   0@0ooo0@0              o@0     0@o    o@o              00ooooo00   o@o                          ║" << std::endl;
    std::cout << "                      ║   0@     @@    0000000  o0@@000o0@@000o o@o   o000000o  o@0          o@000000o   o0   o0000000o   ║" << std::endl;
    std::cout << "                      ║   0@0000@@o        o0@0  o@0     0@o    o@o  0@o    0@o  o0@0000o    o@0    0@   0@o  0@o    0@   ║" << std::endl;   
    std::cout << "                      ║   0@      0@o o0000o0@0  o@0     0@o    o@o  0@oooooo0o       oo0@0  o@o    0@o  0@o  0@o    0@o  ║" << std::endl;   
    std::cout << "                      ║   0@     o0@o @@    o@0   @0     0@o    o@o  0@0     o  oo      0@0  o@o    0@o  0@o  0@o   o@@   ║" << std::endl;
    std::cout << "                      ║   00000000o   o0000000o   o0000o o0000o o0o   o0000000  o00000000o   o0o    o0   o0   0@00000o    ║" << std::endl;
    std::cout << "                      ║                                                                                       0@o         ║" << std::endl;
    std::cout << "                      ║                                                                                       o0          ║" << std::endl;
    std::cout << "                      ╠═══════════════════════════════════════════════════════════════════════════════════════════════════╣" << std::endl;
    printEmptyLine(4);
}

void Basic_Interface::printLinhaMenu(std::string line, std::string orientacao)
{
    if(orientacao == "centro")
    {   
        int ver = 0;

        std::cout << "                      ║";

        if(strlen( line.c_str() ) % 2 == 1 )
            ver = 1;

        for(unsigned int i = 0; i < 50 - ( (strlen( line.c_str() ) ) / 2) ; i++)
            std::cout << " ";
        
        std::cout << line;

        for(unsigned int  i = 0; i < 49 - ((strlen( line.c_str() ) ) / 2) - ver; i++ )
            std::cout << " ";
        
        std::cout << "║" << std::endl;

    }
    if(orientacao == "centralizado"){

        std::cout << "                      ║                                       " << line;
        
        for(unsigned int i = 0; i < 100-(40 + strlen(line.c_str())); i++)
            std::cout << " ";

        std::cout << "║" << std::endl;
    }

    if(orientacao == "esquerda"){
        
        if(strlen(line.c_str()) == 1)
        
            printEmptyLine(1);
        
        else{

            std::cout << "                      ║ " << line;

            for(unsigned int i = 0; i < 99 - (1 + strlen(line.c_str())); i++)
        
                std::cout << " ";
    
            std::cout << "║" << std::endl;
        }
    }    
}

void Basic_Interface::printRodaPe(int linhas)
{
    
    printEmptyLine(25 - linhas);
    
    std::cout << "                      ║  Developed by: João Vitor Alves                     Universidade de Brasília - Faculdade do Gama  ║" << std::endl;
    
    std::cout << "                      ╚═══════════════════════════════════════════════════════════════════════════════════════════════════╝" << std::endl;     
}

void Basic_Interface::printEmptyLine(int linhas)
{
    for(int i = 0; i < linhas; i++)

        std::cout << "                      ║                                                                                                   ║" << std::endl;

}

void Basic_Interface::printAll(std::vector<std::string> text,std::string direcao, int num_linhas)
{
    printLogo();

    for (auto i = text.begin(); i != text.end(); ++i)

        printLinhaMenu(*i,direcao);

    printRodaPe(num_linhas);
}

void Basic_Interface::printAll(std::string line)
{
    printLogo();

    printLinhaMenu(line,"centro");

    printRodaPe(1);
}

void Basic_Interface::screenClear()
{
#if defined _WIN32
    system("cls");
#elif defined (__LINUX__) || defined(__gnu_linux__) || defined(__linux__)
    system("clear");
#elif defined (__APPLE__)
    system("clear");
#endif
}

void Basic_Interface::showAviso(){
    screenClear();
    std::cout << "------------------------------IMPORTANTE, EXECUTE O JOGO MAXIMIZADO------------------------------\n";
    usleep(2000000);
}