#ifndef SUBMARINO_HPP
#define SUBMARINO_HPP

#include "../inc/ship.hpp"

class Submarino : public Ship{
    private:
   
        std::string hability;

    public:

        Submarino();
        Submarino(std::string direction, int x, int y);
        ~Submarino();

        void setHability(std::string hability);
        std::string getHability();

};

#endif