#include "../inc/functions.hpp"

Functions::Functions(){}
Functions::~Functions(){}

void Functions::listFiles(std::string tipo){ //função para listar arquivos do diretório doc/, onde ficam armazenados os mapas e players
DIR *dir;
int lines = 1; //conta numero de linhas somente para formatação da interface
struct dirent *ent;

screenClear();
if(tipo == "Map")

    text_line.push_back("Lista de mapas criados:");

if(tipo == "Player")

    text_line.push_back("Liste de players criados:");

if ((dir = opendir ("doc/")) != NULL) {

  while ((ent = readdir (dir)) != NULL) {
        //função para não listar arquivos que começam com '.' ou os arquivos de player e o arquivo do ranking
        if(ent->d_name[0] == '.' || ent->d_name[strlen(ent->d_name) - 1] == 'g') 

            continue;

        if(tipo == "Player" && ent->d_name[strlen(ent->d_name)-1] == 'r')
        {    
            lines ++;

            ent->d_name[strlen(ent->d_name)-7] = '\0';

            text_line.push_back(ent->d_name);
        
        }
        else if(tipo == "Map" && ent->d_name[strlen(ent->d_name)-1] == 't')
        {
            lines++;

            ent->d_name[strlen(ent->d_name)-4] = '\0';

            text_line.push_back(ent->d_name);
        }
  }

  printAll(text_line,"centralizado", lines);

  text_line.clear();

  closedir (dir);
  
} else 

  perror ("Diretório 'doc' não encontrado.\n");

}

void Functions::criarJogador(Player *player){ //Cria jogador por memória e ao mesmo tempo salva na pasta doc/
    
    screenClear();

    printAll("Digite o nome do player.");

    std::cout << "                      Nome do player: ";
    
    std::string nome;
    
    std::cin >> nome;

    player->setNome(nome);

    player->salvarPlayer();
}


Canoa Functions::criarCanoa(Map *map)
{
    int x,y;
    std::string direction;
    Canoa canoa;

    do{
        
        screenClear();
        
        printAll("Digite o eixo x da canoa:");
        
        std::cout << "                      Eixo x:";
        
        std::cin >> x;
        
        canoa.setX(x);

        
        
        screenClear();
        
        printAll("Digite o eixo y da canoa:");
        
        std::cout << "                      Eixo y: ";
        
        std::cin >> y;
        
        canoa.setY(y);
        
        
        
        screenClear();

        canoa.setDirection("nada");

        if(!verificaPosicao(canoa, *map) )
        {
            screenClear();
            
            printAll("Posicao ou direcao invalidos!");

            usleep(1500000);
        }
    }while(!verificaPosicao(canoa, *map));

    map->setShip(canoa);

    return canoa;
}

Submarino Functions::criarSubmarino(Map *map)
{
    int x,y;
    std::string direction;
    Submarino submarino;

    do{
        screenClear();
        
        printAll("Digite o eixo x do submarino:");
        
        std::cout << "                      Eixo x:";
        
        std::cin >> x;
        
        submarino.setX(x);

        
        
        screenClear();
        
        printAll("Digite o eixo y do submarino:");
        
        std::cout << "                      Eixo y: ";
        
        std::cin >> y;
        
        submarino.setY(y);
        


        screenClear();
        
        printAll("Digite a direcao do submarino:");
        
        std::cout << "                      Direção: ";
        
        std::cin >> direction;
        
        std::transform(direction.begin(), direction.end(), direction.begin(), ::tolower);
        
        submarino.setDirection(direction);

        if(!verificaPosicao(submarino , *map) )
        {
            screenClear();
            
            printAll("Posicao ou direcao invalidos!");

            usleep(1500000);
        }

    }while(!verificaPosicao(submarino, *map));
    
    map->setShip(submarino);

    return submarino;
}

Pavioes Functions::criarPavioes(Map *map)
{
    int x,y;
    std::string direction;
    Pavioes pavioes;

    do
    {
        screenClear();

        printAll("Digite o eixo x do Porta-avioes:");

        std::cout << "                      Eixo x:";

        std::cin >> x;

        pavioes.setX(x);



        screenClear();

        printAll("Digite o eixo y do Porta-avioes:");

        std::cout << "                      Eixo y: ";

        std::cin >> y;

        pavioes.setY(y);



        screenClear();

        printAll("Digite a direcao do Porta-avioes:");

        std::cout << "                      Direção: ";

        std::cin >> direction;
        
        std::transform(direction.begin(), direction.end(), direction.begin(), ::tolower);

        pavioes.setDirection(direction);
        if( !verificaPosicao(pavioes, *map) )
        {
            screenClear();
            
            printAll("Posicao ou direcao invalidos!");

            usleep(1500000);
        }
    }while(!verificaPosicao(pavioes, *map));

    map->setShip(pavioes);

    return pavioes;
}

bool Functions::verificaPosicao(Ship ship, Map map){
    if(ship.getName() == "Canoa")
    {
        if (map.getShip(ship.getX() , ship.getY()) != "" || ship.getX() >= map.getColumns() || ship.getY() >= map.getLines() || ship.getX() < 0 || ship.getY() < 0)

            return false;
    }
    
    else
    {
        for(int x = ship.getX(), y = ship.getY(), i = 0; i < ship.getSize(); i++)
        {    
            
            if(map.getShip(x, y) != "" || x >= map.getColumns() || y >= map.getLines() || x < 0 || y < 0)
                return false;            
            
            if(ship.getDirection() == "direita")
                x++;

            if(ship.getDirection() == "esquerda")
                x--;

            if(ship.getDirection() == "cima")
                y--;

            if(ship.getDirection() == "baixo")
                y++;
        }
    }

    return true;
}

