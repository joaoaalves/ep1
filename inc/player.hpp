#ifndef PLAYER_HPP
#define PLAYER_HPP

#include <string>
#include <fstream>

class Player{
    private:
        std::string nome;
        int jgsganhos;
        int jgsperdidos;
        int totalJgs;
        int posicao;

    public:
        Player();
        ~Player();
        void setNome(std::string nome);
        std::string getNome();
        
        void setJgsganhos(int jgsganhos);
        int getJgsganhos();

        void setJgsPerdidos(int jgsperdidos);
        int getJgsperdidos();
        
        void setPosicao(int posicao);
        int getPosicao();
        
        void setTotalJgs(int totaljgs);
        int getTotalJgs();
        
        
        void salvarPlayer();
};

#endif