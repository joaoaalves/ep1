#include "../inc/player.hpp"

Player::Player(){

    setJgsganhos(0);
    setTotalJgs(0);

}

Player::~Player(){}

void Player::setNome(std::string nome){
    this-> nome = nome;
}

std::string Player::getNome(){
    return nome;
}


void Player::setJgsganhos(int jgsganhos){
    this-> jgsganhos = jgsganhos;
}

int Player::getJgsganhos(){
    return jgsganhos;
}

void Player::setJgsPerdidos(int jgsperdidos)
{
    this -> jgsperdidos = jgsperdidos;
}

int Player::getJgsperdidos(){
    return jgsperdidos;
}
void Player::setTotalJgs(int totaljgs)
{
    this -> totalJgs = totaljgs;
}
int Player::getTotalJgs(){
    return totalJgs;
}

void Player::setPosicao(int posicao)
{
    this -> posicao = posicao;
}

int Player::getPosicao(){
    return posicao;
}


void Player::salvarPlayer(){
    std::ofstream file("doc/" + getNome() + ".player");

    file << "# Jogos ganhos:" << std::endl;

    file << getJgsganhos() << std::endl << std::endl;

    file << "# Total de jogos:" << std::endl;

    file << getTotalJgs();

    file.close();
}