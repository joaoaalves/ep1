#include "../inc/menu.hpp"

using namespace std;

int main(){
    Menu menu;

    int escolha = 0;

    menu.showAviso();

    while(true)
    {
        menu.showMenu();
        
        cin >> escolha;
    
        if(escolha == 1)
        {
            menu.newGame();
        }

        if(escolha == 2)
        {
            menu.salvarNovoJogador();
        }

        if(escolha == 3)
        {
            menu.showRanking();
        }

        if(escolha == 4)
        {
            menu.showInfo();
            continue;
        }

        if(escolha == 5)
        {
            menu.showAjuda();
            continue;
        }
        
        if(escolha == 6)
        {
            char opt;

            do
            {
                menu.screenClear();
                
                menu.printAll("Tem certeza?");

                std::cout << "                       Escolha(S/N): ";
                
                std::cin >> opt;

                opt = toupper(opt);
                
                if(opt == 'S')
                
                    exit(EXIT_SUCCESS);
            }while(opt != 'S' && opt != 'N');
        }
    }

    return 0;
}
