#ifndef INTERFACE_MENU_HPP
#define INTERFACE_MENU_HPP

#include <../inc/basic_interface.hpp>
#include <../inc/functions.hpp>
#include <algorithm>

class   Menu : public Functions
{
public:
    Menu();

    ~Menu();

    void showMenu();
    //printa o menu inicial do jogo
    
    void newGame();

    void showInfo();
    //mostra as informações iniciais do jogo
    
    void showAjuda();
    //mostra ajudas aos iniciantes

    void salvarNovoJogador();
    //Ferramenta para criação de jogadores

    void startGame(Map *map1, Map *map2);
    //Menu em que o jogo começa

    bool verificaBarcos(Map map);
    //Verifica se ainda existem embarcações

    void ataque(Map *map, int x, int y, std::map<std::pair<int, int>, int> *maptent);
    //Realiza ataque no mapa

    void printMapaTentativas(std::map<std::pair<int, int>, int> maptent, Map map);
    //Mostra o mapa de tentativas do player

    void setMapaTentativas(std::map<std::pair<int, int>, int> maptent, Map map);
    //Inicia o mapa de tentativas do player

    void winGame(Player vencedor, Player perdedor);
    //Adciona uma vitória ao vencedor e uma derrota ao perdedor

    void showRanking();
    //Mostra o ranking 
    
};

#endif