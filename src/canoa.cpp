#include <../inc/canoa.hpp>

Canoa::Canoa(){
    setName("Canoa");

    setSize(1);

    setDurability(1);

    setInfo("A canoa é a embarcação mais simples que possui apenas um espaço de tamanho");

    setDirection("Nada");
}

Canoa::Canoa(int x, int y){
    Canoa();

    setX(x);

    setY(y);
}


Canoa::~Canoa(){}