#include "../inc/submarino.hpp"

Submarino::Submarino(){
    setName("Submarino");
    
    setInfo("O submarino é uma embarcação que navega pelo fundo do mar e ocupa 2 espaços");

    setHability("O submarino é revestido por um material que resiste sempre 2 ataques");

    setSize(2);

    setDurability(4);
}

Submarino::Submarino(std::string direction, int x, int y){
    Submarino();

    setDirection(direction);

    setX(x);

    setY(y);
}

Submarino::~Submarino(){}

void Submarino::setHability(std::string hability){
    this-> hability = hability;
}

std::string Submarino::getHability(){
    return hability;
}
